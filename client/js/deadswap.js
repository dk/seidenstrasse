/**************************************************
*
* OCTO Capslock: Enterprise Pneumatic System
* Dmytri Kleiner <dk@telekommunisten.net>
*
* http://octopost.me
*
* This program is free software.
* It comes without any warranty, to the extent permitted by
* applicable law. You can redistribute it and/or modify it under the
* terms of the Do What The Fuck You Want To Public License v2.
* See http://wtfpl.net for more details.
*
* OCTO is a miscommunication technology by Telekommunisten
*
***********************************/

/* jshint indent:2, browser: true, enforceall: true, nocomma: false, forin: false */
/* global cordova */

window.deadSwap = function () {
  "use strict";

  var VERSION = '1.0';

  var terminal = document.getElementById('terminal');
  var output = terminal.querySelector('output');
  var input = terminal.querySelector('#input-line .cmdline');

  var write = function (strings) {
    output.insertAdjacentHTML('beforeEnd', strings.join(''));
  };

  var help = function () {
    var html = '';
    for (var key in commands) {
      if (typeof commands[key].help === 'string') {
        html = html + '<tr><td><strong>' + key + '</strong>&nbsp;</td><td>' + commands[key].help + '</td></tr>';
      }
    }
    write([ '<table>', html, '</table>' ]);
  };

  var commands = {
    help: { command: function () { help(); }}
  };

  var addCommand = function (name, help, command) {
    commands[name] = { help: help, command: command };
  };

  var run = function (cmd, args) {
    if (typeof commands[cmd] === 'object') {
      args = args || [];
      commands[cmd].command(args);
    } else if (cmd) {
      write([ cmd, ' command not found<br>' ]);
    }
  };

  document.addEventListener('click', function () {
    input.focus();
  }, false);

  input.addEventListener('keydown', function (e) {

    var entry = this.value.trim();

    if (e.keyCode === 13) { // enter

      var line = this.parentNode.parentNode.cloneNode(true);
      line.removeAttribute('id');
      line.classList.add('line');
      var echo = line.querySelector('input.cmdline');
      echo.autofocus = false;
      echo.readOnly = true;
      output.appendChild(line);

      var cmd;
      var args;
      if (entry) {
        args = entry.split(' ');
        cmd = args[0].toLowerCase();
        args = args.splice(1);
      }

      run(cmd, args);
      this.value = '';
    }
    input.scrollIntoView(true);
  }, false);

  addCommand('clear', 'clear terminal', function () {
    output.innerHTML = '';
    input.value = '';
  });

  addCommand('monitor', 'open monitor', function () {
    Router.go('/monitor');
  });

  addCommand('date', 'show current date', function () {
    write([ (new Date()).toLocaleString(), '<br>' ]);
  });

  addCommand('instructions', 'show instructions', function () {
    write([ '<br>' ]);
    write([ '<div>Welcome to ', document.title, '! (v', VERSION, ')<br>Enterprise Pneumatic System</div>' ]);
    write([ '<br>' ]);
    write([ 'To request a capsule to a station use the request command<br>' ]);
    write([ 'usage: request [station #]<br>' ]);
    write([ 'i.e. request 1<br>' ]);
    write([ '<br>' ]);
    write([ 'To send a capsule from one station to another use the send command<br>' ]);
    write([ 'usage: send [origin station #] [destination station #]<br>' ]);
    write([ 'i.e. send 1 3<br>' ]);
    write([ '<br>' ]);
  });

  addCommand('request', 'request capsule', function (args) {
    if (args.length === 1 && !isNaN(args[0])) {
      Messages.insert({
        createdAt: new Date(),
        message: 'capsule request from station ' + args[0]
      });
      write([ 'capsule request sent<br>' ]);
      write([ 'your capsule will arrive shortly<br>' ]);
    } else {
      write([ 'usage: request [station #]<br>' ]);
      write([ 'i.e. request 1<br>' ]);
    }
  });

  addCommand('send', 'send capsule', function (args) {
    if (args.length === 2 && !isNaN(args[0]) && !isNaN(args[1])) {
      Messages.insert({
        createdAt: new Date(),
        message: 'send from station ' + args[0] + ' to station ' + args[1]
      });
      write([ 'send request sent<br>' ]);
      write([ 'insert capsule<br>' ]);
    } else {
      write([ 'usage: send [origin station #] [destination station #]<br>' ]);
      write([ 'i.e. send 1 3<br>' ]);
    }
  });

  addCommand('credits', 'show credits', function (args) {
    write([ '<br><b>OCTO: A Global Pipe Dream Come True</b><br>2013-2015<br><br>' ]);
    write([ '<b>Telekommunisten</b><br>' ]);
    write([ '&nbsp;&nbsp;Jeff Mann, Baruch Gottlieb, Jonas Frankki<br>' ]);
    write([ '&nbsp;&nbsp;Diani Barreto, Dmytri Kleiner, Rico Weise, Mike Pearce<br>' ])
    write([ '<b>transmediale</b><br>' ]);
    write([ '&nbsp;&nbsp;Tatiana Bazzichelli, Kristoffer Gansing, Markus Huber<br>' ]);
    write([ '&nbsp;&nbsp;Inga Seidler, Georgia Nicolau, Heiko Stubenrauch<br>' ]);
    write([ '<b>raumlabor</b><br>' ]);
    write([ '&nbsp;&nbsp;Andrea Hofmann, Markus Bader, Florian Stirnemanne<br>' ]);
    write([ '<b>Haus der Kulteren der Welt</b><br>' ]);
    write([ '&nbsp;&nbsp;Sibylle Kerlisch, Phillip Sn&uuml;derhauf<br>' ]);
    write([ '<b>PNEUMAtic circUS</b><br>' ]);
    write([ '&nbsp;&nbsp;Vittori Baroni, Carlo Galli, Giovanni Galli<br>' ]);
    write([ '&nbsp;&nbsp;Gaia Querci, Tatiana Villani, Manuel Perna, MGZ<br>' ]);
    write([ '&nbsp;&nbsp;Topsy Qur\'et, Theis Vall&oslash; Madsen, Lutz Wohlrab, Karla Sachse<br>' ]);
    write([ '&nbsp;&nbsp;with 100+ contributing artists<br>' ]);
    write([ '<b>Seidenstrasse / CCC</b><br>' ]);
    write([ '&nbsp;&nbsp;Mey Lean Kronemann, Frank Rieger<br>' ]);
    write([ '&nbsp;&nbsp;with the global hacker community.<br>' ]);
    write([ '<b>New Babylon Revisited</b><br>' ]);
    write([ '&nbsp;&nbsp;Sofia Dona, Daphne Dragona<br>' ]);
    write([ '<b>Errands</b><br>' ]);
    write([ '&nbsp;&nbsp;Dimitris Theodoropoulos, Maria Tsigara<br>' ]);
    write([ '<b>Aksioma / &#352kuc</b><br>' ]);
    write([ '&nbsp;&nbsp;Janez Jan&#353;a, Vladimir Vidmar, Marcela Okreti&#269;<br>' ]);
    write([ '&nbsp;&nbsp;Jo&#353;ko Pajer, Sonja Grdina, Boris Bej<br>' ]);
    write([ '&nbsp;&nbsp;Atila Bo&#353;tjan&#269;i&#269;, Valter Udovi&#269;i&#263;<br>' ]);
    write([ '<br>More Miscommunications Technologies from Telekommunisten:<br>' ]);
    write([ '&nbsp;&nbsp;Miss Communication (2008), deadSwap (2009, 2015), Thimbl (2010), R15N (2011-2012),<br>' ]);
    write([ '&nbsp;&nbsp;iMine (2012), Miscommunication Station (2012, 2013), Numbers Station (2014)<br>' ]);
    write([ '<br>Publications:<br>' ]);
    write([ '&nbsp;&nbsp;Networked Disruption, Tatiana Bazzichelli<br>' ]);
    write([ '&nbsp;&nbsp;Gratitude for Technology, Baruch Gottlieb<br>' ]);
    write([ '&nbsp;&nbsp;Telekommunist Manifesto, Dmytri Kleiner<br>' ]);

  });

  addCommand('exit', 'close OCTO Capslock', function () {
    Router.go('/');
  });

  addCommand('logout', 'logout of OCTO Capslock', function () {
    Session.set('PIN', undefined);
  });

  addCommand('message', ' message to the operator', function (args) {
    if (args.length !== 0) {
      Messages.insert({
        createdAt: new Date(),
        message: args.join(' ')
      });
      write([ 'message sent<br>' ]);
    } else {
      write([ 'usage: message [message text]<br>' ]);
      write([ 'i.e. message Help, need assistance at Station 1.<br>' ]);
    }
  });

  write([ '<div>Welcome to ', document.title, '! (v', VERSION, ')<br>Enterprise Pneumatic System</div>' ]);
  run('date');

  write([ '<p>Documentation: type "help"</p>' ]);
  input.focus();
};

/* OCTO Capslock by Telekommunisten */
// vim: tabstop=2 shiftwidth=2 expandtab
