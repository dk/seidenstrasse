/* Router.configure({
  waitOn: function() {
    if (Meteor.userId()) {
      Tracker.autorun(function() {
        // Put Subscriptions Here
      });
    }
  }
}); */

Router.route('/', function() {
  this.render('about');
});

Router.route('/about', function() {
  this.render('about');
});

Router.route('/monitor', function() {
  if ('1511' === Session.get('PIN')) {
    this.render('monitor');
  } else {
    this.render('security');
  }
});

Router.route('/control', function() {
  if ('1511' === Session.get('PIN')) {
    this.render('control');
  } else {
    this.render('security');
  }
});

Router.route('/security', function() {
  this.render('security');
});

